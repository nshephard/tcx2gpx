# Contributing

Contributions are welcome! To contribute you should [fork the
repository](https://gitlab.com/nshephard/tcx2gpx/-/forks/new) and then clone your fork locally to work on it. Assuming
you do not rename the repository when you fork it replace `<username>` in the following with your GitLab username. Then
install in editable mode along with the development and test requirements.

``` bash
git clone git@gitlab.com:<username>/tcx2gpx.git
pip install -e .[dev,tests]
```

Make changes on your fork and ideally include tests where possible. Once ready create a [merge
request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).
