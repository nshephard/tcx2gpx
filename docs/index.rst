Welcome to tcx2gpx documentation!
=================================

.. toctree::
   :maxdepth: 1
   :caption: Getting Started

   introduction
   installation
   usage
   contributing

.. toctree::
   :maxdepth: 2
   :caption: API

   tcx2gpx.batch
   tcx2gpx.tcx2gpx

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
